init=()=>{
// считует товар и вызывает ф-цию goodsOut
  $.post(
  'admin/core.php',
  {
      'action' : 'loadGoods'
      },
      goodsLaterOut
      );  
}

function goodsLaterOut(data){
  data=JSON.parse(data);
  let out='';
  let later={};
    if(localStorage.getItem("later")){ //если существует то записуем в желания
        later = JSON.parse(localStorage.getItem("later")); 
        for(let key in later){
           out +='<div class="cart">';
           out +=`<p class="name">${data[key].name}</p>`;
           out +=`<img src="/images/${data[key].img}"></img>`;
           out +=`<div class="cost">${data[key].cost}</div>`;
           out +=`<a href='goods.html#${key}'>Посмотреть</a>`;
           out +='</div>';
  }
  $('.goods-out').html(out);
    } else {
        //если желаний нет то просто выведем на страницу 
        $('.goods-out').html('Добавте товар в желания');
    }
};


//после загрузки страницы
$(()=>{
  init();
});

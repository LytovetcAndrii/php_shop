// JS обслуживает главную страницу
// корзина будет хранить id и количество товара по id
let cart={};

init=()=>{
// считует товар и вызывает ф-цию goodsOut
//getJSON - сокращенная функция Ajax
  //$.getJSON('goods.json',goodsOut);
  $.post(
  'admin/core.php',
  {
      'action' : 'loadGoods'
      },
      goodsOut
      );  
}





function goodsOut(data){
//вывод товаров на главную страницу
//нужно проверить что приходит в data приходит массив 
//console.log(data);    
  data=JSON.parse(data);
  let out='';
  for(let key in data){
   out +='<div class="cart">';
   out +=`<button class="add-to-later" data-id="${[key]}">&reg;</button>`;
   out +=`<p class="name">${data[key].name}</p>`;
   out +=`<img src="/images/${data[key].img}"></img>`;
   out +=`<div class="cost">${data[key].cost}</div>`;
   out +=`<button class="add-to-cart" data-id="${[key]}">Купить</button>`;
   out +='</div>';
  }
  $('.goods-out').html(out);
  $('.add-to-cart').on('click',addToCart);
  $('.add-to-later').on('click',addToLater);
};

// Стрелочная функция для this не работает так как для стрелочной функции this это не ссылка на элемент источника 

function addToCart(){
let id =$(this).attr('data-id');
  if(cart[id]==undefined){
      cart[id]=1;
    }
    else cart[id]++;
//  console.log(cart);
  saveCart();
  showMiniCart();
};

function addToLater(){ // добавляю товар в желания
    let later = {};
    if(localStorage.getItem("later")){ //если существует то записуем в желания
    later = JSON.parse(localStorage.getItem("later")) 
    }
    alert('Добавлено в желания');
    let id =$(this).attr('data-id');
    later[id]=1; //пока сохранили в массив 
    //save to Local Storage
    try{
            localStorage.setItem('later', JSON.stringify(later));
        }
    catch(e){
          if (e == QUOTA_EXCEEDED_ERR){ 
              alert('Превышен лимит 5Mb');
        }
   } 
}

showMiniCart = ()=>{
  let out='';
  for(let key in cart){
    out += key + ' ---- ' + cart[key] + '<br>';
  }
  $('.mini-cart').html(out);
};

saveCart = ()=>{
  // так как не может хранить объект то преобразуем в строку
  //console.log("save"); // localStorage.setItem('ключ', 'значение')
  try {
  let serialObj = JSON.stringify(cart);
  localStorage.setItem('cart',serialObj);
      } catch (e) {
          if (e == QUOTA_EXCEEDED_ERR){ 
              alert('Превышен лимит 5Mb');
              }
      }
};

loadCart = ()=>{
// получим товары с карзины 
if(localStorage.getItem("cart")){
cart = JSON.parse(localStorage.getItem("cart")) 
showMiniCart();}
};

//после загрузки страницы
$(()=>{
  init();
  loadCart();
});

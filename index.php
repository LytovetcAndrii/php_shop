<?php
error_reporting(-1);
ini_set('display_errors','on');
header('Content-Type: text/html; charset=utf-8');

$dat=date("l jS \of F Y h:i:s A");
echo "$dat";

require("components/header.php");
require('components/main.php');
require('components/footer.php');
?>
    <script src="vendor/components/jquery/jquery.js"></script> 
    <script src="js/main.js"></script>
</html>

<?php
error_reporting(-1);
ini_set('display_errors','on');
header('Content-Type: text/html; charset=utf-8');

function connect(){ // подключение к БД
  $servername = "localhost";
  $username = "postgres";
  $password = "postgres";
  $dbname = "store";
  $connection_string='host='.$servername.' '.'port=5432'.' '.'user='.$username.' '.'password='.$password.' '.'dbname='.$dbname;

    $conn=$connection_string;
    $conn=pg_connect($connection_string) or die ('нет соединения');
 // $conn=pg_connect("host=localhost port=5432 user=postgres password=postgres dbname=store") or die ('нет соединения');
  //$conn=pg_connect("localhost","postgres","postgres","store");
/*
    if(!$conn){
        die ('Connection failed'.pg_last_error());
    }
*/
    //echo pg_last_error($conn);
// пробывал менять кодировку
//pg_set_client_encoding($conn, "utf8");
    return $conn;
}

function init(){ //получения id name товара для отображения
//echo 'work function.php';
//вернет дeскриптор подключения 
    $conn=connect();
    $sql="SELECT id,name FROM goods";
    $result = pg_query($conn, $sql);

    if(pg_num_rows($result)>0){
        $out = array();
        while ($row = pg_fetch_assoc($result)) {
            //$out[]=$row;   
            $out[$row["id"]]=$row;   
            }
        //запаковываем массив в строку
        echo json_encode($out);
    } else echo "0";

    pg_close($conn);    
}

function selectOneGoods(){// в admin.html при выборе из select получим данные с БД по id
//вернет дeскриптор подключения 
    $conn=connect();
    //echo 'connect selectOneGoods ';
    $id = $_POST['id'];
   $sql="SELECT * FROM goods WHERE id = '$id'";

    $result = pg_query($conn, $sql);

    if(pg_num_rows($result)>0){
        $row = pg_fetch_assoc($result);
        // передать можно только строку, вернет строку содержащую json представление для указанной переменной. 
        echo json_encode($row);
    } else echo "0";

    pg_close($conn);    
}

function updateGoods(){
//echo 'function updateGoods';
    $conn=connect();
//получаем данные с POST
    $id = $_POST['id'];
    $name = $_POST['gname'];
    $cost = $_POST['gcost'];
    $descr = $_POST['gdescr'];
    $ord = $_POST['gorder'];
    $img = $_POST['gimg'];
//переменные нужно взять в кавычки
    $sql="UPDATE goods SET name='$name', cost='$cost', description='$descr', ord='$ord', img='$img' WHERE id ='$id' "; 
    // так не работает, нужно передавать объект
    //$result = pg_update($conn, $sql);
    $result = pg_query($conn, $sql);

    if($result){
        echo 1;
        } else echo 0; pg_close($conn);    
    //writeJSON();
}

function newGoods(){
    //echo 'work newGoods   new';
    $conn=connect();
//получаем данные с POST
    $name = $_POST['gname'];
    $cost = $_POST['gcost'];
    $descr = $_POST['gdescr'];
    $ord = $_POST['gorder'];
    $img = $_POST['gimg'];
//переменные нужно взять в кавычки
   $sql="INSERT INTO goods(name, cost, description, ord, img) VALUES ('$name', '$cost', '$descr', '$ord', '$img')"; 

    $result = pg_query($conn, $sql);

    if($result){
        echo 1;
    } else echo 0;

    pg_close($conn);    
    //writeJSON();
}

function writeJSON(){ //для записи в файл goods.json при изменении БД (в дальнейшем не используется)
    //echo "work writteJSON";
    $conn=connect();
    $sql="SELECT * FROM goods";
    $result = pg_query($conn, $sql);

    if(pg_num_rows($result)>0){
        $out = array();
        while ($row = pg_fetch_assoc($result)) {
            //$out[]=$row;   
            $out[$row["id"]]=$row;   
            }
    //echo "work writteJSON";
        $a=file_put_contents("../goods.json",json_encode($out));
        echo $a;

    } else echo 0;
    pg_close($conn);    
}

function loadGoods(){
    //echo "work loadGoods";
    $conn=connect();
    $sql="SELECT * FROM goods";
    $result = pg_query($conn, $sql);

    if(pg_num_rows($result)>0){
        $out = array();
        while ($row = pg_fetch_assoc($result)) {
            $out[$row["id"]]=$row;   
            }
        echo json_encode($out);
    } else echo 0;
    pg_close($conn);    
}

function loadSingleGoods(){ //для извлечения из базы описание товара по id
    $id = $_POST['id'];
    $conn=connect();
    // echo 'connect selectOneGoods '.$id;
    $sql="SELECT * FROM goods WHERE id = '$id'";
    $result = pg_query($conn, $sql);
    if(pg_num_rows($result)>0){
        $row = pg_fetch_assoc($result);
        echo json_encode($row);
    } else echo "0";

    pg_close($conn);    
}

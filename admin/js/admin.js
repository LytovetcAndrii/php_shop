function init(){// отправляем на core.php запроса 'action':'init'; вызов showGoods
    $.post(
        "core.php",
        {
            "action": "init"
        },
        showGoods
    );
}

function showGoods(data){// добавление в select названия товара и класса data-id = '${id}'
// json in array
    data=JSON.parse(data);
    //console.log(data);
    let out='<select>';
    out += '<option data-id="0">Новый товар</option>';
    for(let id in data){
    out += `<option data-id='${id}'>${data[id].name}</option>`;
    }
    out += '</select>';
    $('.goods-out').html(out);
//после того как товар показан на странице добавим событие на select
    $('.goods-out select').on('change',selectGoods);
    }

function selectGoods(){// получаем id товара и вытаскиваем из БД
    let id = $('.goods-out select option:selected').attr('data-id');
// нужно вычитать все поля товара по id из БД
    $.post(
        'core.php',
        {
            "action" : "selectOneGoods",
            "id" : id
        },
        function(data){
        //разбирает строку JSON в объект
         //console.log(data);
         data=JSON.parse(data);
         $('#gid').val(data.id); 
         $('#gname').val(data.name); 
         $('#gcost').val(data.cost); 
         $('#gdescr').val(data.description); 
         $('#gorder').val(data.ord); 
         $('#gimg').val(data.img); 
                } 
    );
}

function saveToDb(){// после нажатия кнопки редактирования товара данные запишутся в БД
    let id = $('#gid').val();
    if(id !=""){
        $.post( 'core.php',
            {
                'action':'updateGoods',
                'id':id,
                'gname': $('#gname').val(),
                'gcost':$('#gcost').val(), 
                'gdescr':$('#gdescr').val(),
                'gorder':$('#gorder').val(), 
                'gimg':$('#gimg').val()
            },function(data){
            //console.log(data);
            if(data==1){
                alert("Данные изменены");
                init();
            } else console.log(data);
        }
    );
        
        } else {
        //console.log("NewGoods");
        $.post(
            'core.php',
            {
                'action':'newGoods',
                'gname': $('#gname').val(),
                'gcost':$('#gcost').val(), 
                'gdescr':$('#gdescr').val(),
                'gorder':$('#gorder').val(), 
                'gimg':$('#gimg').val()
            },function(data){
            if(data==1){
                alert("Запись добавлена");
                init();
            } else console.log(data);
        }
    );
           
        }
}

$(()=>{
  init();
  $('.add-to-db').on('click',saveToDb);
});

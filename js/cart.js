let cart = {};

// получим товары с карзины 
loadCart = ()=>{
if(localStorage.getItem("cart")){
  cart = JSON.parse(localStorage.getItem("cart")) 
  console.log("loadcart до if",cart);
  //проверка объекта на пустоту 
  if(isEmptyObject(cart)){
  console.log("loadcart после  if",cart);
      $('.main-cart').html("Корзина пуста");
    }
      else  showCart();
  }
};

function isEmptyObject(obj) {
for (var i in obj) {
        if (obj.hasOwnProperty(i)) {
            return false;
        }
    }
    return true;
}

showCart = ()=>{
  $.getJSON('goods.json',(data)=>{
    let goods=data;
    //перебираем корзину 
  let out='';
  for(let id in cart){
    out +='<div class="main-cart-item">';
    out += `<div>${goods[id].name}</div>`;
    out += `<img src="images/${goods[id].img}">`;
    out += `<div class="count">${cart[id]}</div>`;
    out += `<button class="delete-goods" data-id="${id}">Delete</button>`;
    out += `<div class="container-inline">`;
    out += `<button class="add-goods" data-id="${id}">Увеличить</button>`;
    out += `<button class="minus-goods" data-id="${id}">Уменьшить</button>`;
    out +='</div>';
    out +='</div>';
    }
    $('.main-cart').html(out);
    $('.delete-goods').on('click',deleteGoods);
    $('.add-goods').on('click',reduceGood);
    $('.minus-goods').on('click',increaseGood);

  });
}

// удаление товара
function deleteGoods(){
  let id=$(this).attr('data-id');
  delete cart[id];
  saveCart();
  loadCart();
}

function reduceGood(){
  let id=$(this).attr('data-id');
  cart[id]++;
  saveCart();
  loadCart();
}
    
//Уменьшить
function increaseGood(){
  let id=$(this).attr('data-id');
  if(cart[id]>1){
  cart[id]--;
  } else  delete cart[id];
  saveCart();
  loadCart();
}

saveCart = ()=>{
// так как не может хранить объект то преобразуем в строку
try {
    let serialObj = JSON.stringify(cart);
    localStorage.setItem('cart',serialObj);
    } catch (e) {
	if (e == QUOTA_EXCEEDED_ERR) {
         alert('Превышен лимит 5Mb');
	   }
    }
};

sendEmail =()=>{

// отправка письма с заказом
  let ename=$('#ename').val();
  let email=$('#email').val();
  let ephone=$('#ephone').val();
//проверка
if(ename != '' && email !='' && ephone != ''){
  if(isEmptyObject(cart)){
      $('.main-cart').html("Корзина пуста");
    }
      else  {
             //  alert('Send message');
                $.post(
                        "core/mail.php",
                        {
                        "ename":ename,
                        "email":email,
                        "ephone":ephone,
                        "cart":cart
                        },
                      function(data){
//отбработка ответа если успешно
console.log(data);
                      }

                        );
                      }
  
  } else alert('Заполнить поля');
}


$(document).ready(()=>{
  loadCart();
  $('.send-email').on('click',sendEmail); //отправить письмо с заказом
});

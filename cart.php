<?php
error_reporting(-1);
ini_set('display_errors','on');
header('Content-Type: text/html; charset=utf-8');

require("components/header.php");
require('components/cart.php');
require('components/footer.php');
?>

    <script src="vendor/components/jquery/jquery.js"></script>
    <script src="js/cart.js"></script>
</html>
